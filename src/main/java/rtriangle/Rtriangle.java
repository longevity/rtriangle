package rtriangle;

import java.awt.Point;

public class Rtriangle {
	public Point a;
	public Point b;
	public Point c;

	private double lengthA;
	private double lengthB;
	private double lengthC;
	
	Rtriangle(Point pa, Point pb, Point pc){
		a = pa;
		b = pb;
		c = pc; 
		
	    lengthA = Point.distance(a.getX(), a.getY(), b.getX(), b.getY());
	    lengthB = Point.distance(b.getX(), b.getY(), c.getX(), c.getY());
	    lengthC = Point.distance(c.getX(), c.getY(), a.getX(), a.getY());
	}
	
	public double getLengthA() {
		return lengthA;
	}
	
	public double getLengthB() {
		return lengthB;
	}
	
	public double getLengthC() {
		return lengthC;
	}
}