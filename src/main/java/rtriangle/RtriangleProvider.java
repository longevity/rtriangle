package rtriangle;

import java.awt.Point;

public final class RtriangleProvider {
	public static Rtriangle getRtriangle(int x1, int x2, int x3,
										 int y1, int y2, int y3) 
	{
		return new Rtriangle(new Point(x1, y1), new Point(x2, y2), new Point(x3, y3));
	}
	
	public static IRtriangle getRtriangle() 
	{
		return new IRtriangle() {
			public int getApexX1() {
				return 0;
			}
			
			public int getApexY1() {
				return 0;
			}
			
			public int getApexX2() {
				return 4;
			}
			
			public int getApexY2() {
				return 0;
			}
			
			public int getApexX3() {
				return 4;
			}
			
			public int getApexY3() {
				return 4;
			}
		};
	}
}
