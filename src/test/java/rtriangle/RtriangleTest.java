package rtriangle;

import java.awt.Point;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class RtriangleTest {
	private Rtriangle rTriangle;
	
	private double sideA;
	private double sideB;
	private double sideC;
	
	@BeforeTest
	public void checkThatItsTriangle() {
		rTriangle = RtriangleProvider.getRtriangle(0, 4, 4, 0, 0, 3);
		
		sideA = rTriangle.getLengthA();
		sideB = rTriangle.getLengthB();
		sideC = rTriangle.getLengthC();
		
		Assert.assertTrue(sideA+sideB > sideC && 
						  sideA+sideC > sideB && 
						  sideB+sideC > sideA, "The sides don't form a triangle.");
		
		boolean isEqualsXab = isEqualInRangeToValue(rTriangle.a.getX(), rTriangle.b.getX(), 0.00001, 5);
		boolean isEqualsYab = isEqualInRangeToValue(rTriangle.a.getY(), rTriangle.b.getY(), 0.00001, 5);
		
		Assert.assertFalse(isEqualsXab && isEqualsYab, "Points a and b matched. Its not triangle.");
		
		boolean isEqualsXac = isEqualInRangeToValue(rTriangle.a.getX(), rTriangle.c.getX(), 0.00001, 5);
		boolean isEqualsYac = isEqualInRangeToValue(rTriangle.a.getY(), rTriangle.c.getY(), 0.00001, 5);
		
		Assert.assertFalse(isEqualsXac && isEqualsYac,"Points a and c matched. Its not triangle.");
					
		boolean isEqualsXbc = isEqualInRangeToValue(rTriangle.b.getX(), rTriangle.c.getX(), 0.00001, 5);
		boolean isEqualsYbc = isEqualInRangeToValue(rTriangle.b.getY(), rTriangle.c.getY(), 0.00001, 5);
			
		Assert.assertFalse(isEqualsXbc && isEqualsYbc, "Points b and c matched. Its not triangle.");	
	}
	
	@Test
    public void testThatItsRtriangle () 
	{
    	double a = Math.pow(sideA, 2);
    	double cb = Math.pow(sideC, 2) + Math.pow(sideB, 2);
    	
    	double b = Math.pow(sideB, 2);
    	double ca = Math.pow(sideC, 2) + Math.pow(sideA, 2); 
    	
    	double c = Math.pow(sideC, 2);
		double ab = Math.pow(sideA, 2) + Math.pow(sideB, 2);
		
		System.out.println("first check...");
		
		SoftAssert softAssert = new SoftAssert();
		
		softAssert.assertTrue(
    			isEqualInRangeToValue(ab, c, 0.00001, 5) || 
    			isEqualInRangeToValue(ca, b, 0.00001, 5) ||
    			isEqualInRangeToValue(cb, a, 0.00001, 5), "This is a not right triangle.");
		System.out.println("This is a right triangle.");

		System.out.println("second check..."); // примечание: здесь не производилась проверка на тип фигуры(треугольник).
    	IRtriangle triangle = RtriangleProvider.getRtriangle();
    	
    	a = Math.pow(Point.distance(triangle.getApexX1(), triangle.getApexY1(), triangle.getApexX2(), triangle.getApexY2()), 2);
    	b = Math.pow(Point.distance(triangle.getApexX2(), triangle.getApexY2(), triangle.getApexX3(), triangle.getApexY3()), 2);
    	c = Math.pow(Point.distance(triangle.getApexX3(), triangle.getApexY3(), triangle.getApexX1(), triangle.getApexY1()), 2);
    	
    	double[] sides = new double[] {a, b, c};
    	
    	Arrays.sort(sides);
    	
    	softAssert.assertTrue(isEqualInRangeToValue(sides[2], sides[0] + sides[1], 0.00001, 5), "This is a not right triangle.");
    	System.out.println("This is a right triangle.");
    	softAssert.assertAll();
	}
	
	/** 
	 * Проверка, что текущее значение индикатора отличается от значения, переданного в аргументе, 
	 * на заданную величину в указанном диапазоне.
	 * @param fValue, sValue Величины, для сравнения их значений.
	 * @param range Диапазон сравнения.
	 * @param precise Требуемая точность округления при сравнении.
	 * @return Величины равны (true) / не равны (false)
	 * */
	public static boolean isEqualInRangeToValue(double fValue, double sValue, double range, int precise) {
		Assert.assertTrue(range >= 0, "Range value " + range + " is negative.");
		return roundResult(fValue - sValue, precise) <= range ;				
	}
	
	/**
	 * Метод округления.
	 * @param value значение типа double, которое требуется округлить.
	 * @param precise требуемая точность округления.
	 * @return значение типа double, округлённое до указанной точности. 
	 */
	private static double roundResult (double value, int precise)
	{
		return new BigDecimal(""+value).setScale(precise, RoundingMode.HALF_UP).doubleValue();
	}
}
